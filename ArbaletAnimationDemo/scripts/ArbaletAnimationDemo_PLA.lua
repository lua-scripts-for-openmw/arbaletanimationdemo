--демонстрационный скрипт для ошибки арбалета при поиске точки центра экрана
--misc_skull10  - череп слуги

local self = require('openmw.self')
--local types = require('openmw.types')
--local nearby = require('openmw.nearby')
--local input = require('openmw.input')
local util = require('openmw.util')
local camera = require('openmw.camera')
local core = require('openmw.core')

local Shot

--функция отслеживания взмаха оружием
local function checkShot()

	if Actor.stance(self) == 1 then  --проверка на позу
		if self.controls.use == 1 then
			Shot = true
				--ui.showMessage('оружие подготовлено') 
		elseif self.controls.use == 0 then
			if Shot == true then
			local SkullPos = camera.getPosition() + camera.viewportToWorldVector(util.vector2(0.5, 0.5))*200
			core.sendGlobalEvent('SkullAdd', {SkullPos=SkullPos, Player = self})
			Shot = false
			end			
		end--конец отслеживания момента выстрела
	end  --конец отслеживания позы
end --конец функции отслеживания выстрела

return {
    engineHandlers = {
	
	onUpdate = function(dt)

	checkShot()
		
	end, --конец onUpdate
	},
	
	eventHandlers = { 
	SkullAdd = SkullAdd, --событие добавления черепа
	}
}