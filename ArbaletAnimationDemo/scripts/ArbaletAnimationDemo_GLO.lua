local world = require('openmw.world')
local core = require('openmw.core')


local function SkullAdd(data)
Skull = world.createObject('misc_skull10')
Cell = data.Player.cell
Skull:teleport(Cell, data.SkullPos) 
end --конец функции появления черепа


return {
	
	eventHandlers = { 
	SkullAdd = SkullAdd, --событие добавления черепа
	}
}