# ArbaletAnimationDemo

This is a script created to demonstrate the error of the camera.viewportToWorldVector function when firing a crossbow. Theoretically, a skull should appear exactly at the point where the player is looking. For melee weapons everything works correctly, but when shooting from a crossbow (less often from a bow), the skull does not appear exactly in the right point.

https://youtu.be/UC9hWVQH3rk